package lt.akademija.jpaexam.ex02associaions;

import lt.akademija.jpaexam.ex02associaions.repositories.BookRepo;
import org.springframework.stereotype.Service;

@Service
public class BookRepository {

    private BookRepo bookRepo;

    public BookRepository(BookRepo bookRepo) {
        this.bookRepo = bookRepo;
    }

    public Book saveOrUpdate(Book e) {
        return this.bookRepo.save(e);
    }

    public Book find(Long bookId) {
        return this.bookRepo.findOne(bookId);
    }
}
