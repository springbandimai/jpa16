package lt.akademija.jpaexam.ex02associaions;

import lt.akademija.jpaexam.ex02associaions.repositories.LibraryRepo;
import org.springframework.stereotype.Service;

@Service
public class LibraryRepository {

    private LibraryRepo libraryRepo;

    public LibraryRepository(LibraryRepo libraryRepo) {
        this.libraryRepo = libraryRepo;
    }

    public Library saveOrUpdate(Library e) {
        return this.libraryRepo.save(e);
    }

    public Library find(Long id) {
        return this.libraryRepo.findOne(id);
    }
}
