package lt.akademija.jpaexam.ex02associaions.controllers;

import lt.akademija.jpaexam.ex02associaions.Book;
import lt.akademija.jpaexam.ex02associaions.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/book")
public class BookController {

    @Autowired
    private BookRepository bookRepository;

    public BookController(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @RequestMapping(path = "/{bookName}", method = RequestMethod.PUT)
    public Book saveOrUpdate(@PathVariable Book e) {
        return this.bookRepository.saveOrUpdate(e);
    }

    @RequestMapping(value = "/{bookId}", method = RequestMethod.GET)
    public Book find(@PathVariable Long bookId) {
        return this.bookRepository.find(bookId);
    }
}
    