package lt.akademija.jpaexam.ex02associaions.controllers;

import lt.akademija.jpaexam.ex02associaions.Library;
import lt.akademija.jpaexam.ex02associaions.LibraryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/library")
public class LibraryController {

    @Autowired
    private LibraryRepository libraryRepository;

    public LibraryController(LibraryRepository libraryRepository) {
        this.libraryRepository = libraryRepository;
    }

    @RequestMapping(path = "/{libraryName}", method = RequestMethod.PUT)
    public Library saveOrUpdate(@PathVariable Library e) {
        return this.libraryRepository.saveOrUpdate(e);
    }

    @RequestMapping(path = "/{libreryId}", method = RequestMethod.GET)
    public Library find(@PathVariable Long id) {
        return this.libraryRepository.find(id);
    }
}
    