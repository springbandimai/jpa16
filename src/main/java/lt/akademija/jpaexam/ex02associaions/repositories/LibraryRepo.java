package lt.akademija.jpaexam.ex02associaions.repositories;

import lt.akademija.jpaexam.ex02associaions.Library;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LibraryRepo extends JpaRepository<Library, Long>{
}
    