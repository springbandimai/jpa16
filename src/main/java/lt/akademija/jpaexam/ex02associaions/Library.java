package lt.akademija.jpaexam.ex02associaions;

import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.List;

@Entity
public class Library {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Simple name of the library
     */
    @Length(min = 1, max = 100)
    private String name;

    /**
     * Readers are people registered to particular library.
     */
    private List<LibraryReader> readers;

    /**
     * Holds all books that are available to borrow in this library
     */
    private List<Book> books;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<LibraryReader> getReaders() {
        return readers;
    }

    public void setReaders(List<LibraryReader> readers) {
        this.readers = readers;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
