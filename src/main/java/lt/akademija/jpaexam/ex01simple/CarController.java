package lt.akademija.jpaexam.ex01simple;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api/car")
public class CarController {

    @Autowired
    private CarRepository carRepository;

    public CarController(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<CarEntity> findAll() {
        return this.carRepository.findAll();
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public CarEntity find(@PathVariable Long id) {
        return this.carRepository.find(id);
    }

    @RequestMapping
    public CarEntity saveOrUpdate(CarEntity e){
        return this.carRepository.saveOrUpdate(e);
    }
}
    